package fr.appstore.spark
import org.apache.spark.sql.{Encoder, Encoders, SparkSession}
import fr.appstore.spark.model.{GooglePlay, AppleAppData}
import org.apache.spark.sql.functions._
import org.apache.spark.rdd.RDD

object Main {

  def main(args: Array[String]): Unit = {
    // On Crée une session Spark
    val spark = SparkSession.builder.appName("Applications Analysis").getOrCreate()
    val sc = spark.sparkContext

    import spark.implicits._

    try {
      // On charge les données depuis les fichiers
      val googlePlayFile = spark.read.textFile(args(0)).cache()
      val appleAppDataFile = spark.read.textFile(args(1)).cache()

      // On définie les encodeurs pour les classes GooglePlay et AppleAppData
      implicit val googlePlayEncoder: Encoder[GooglePlay] = Encoders.bean[GooglePlay](classOf[GooglePlay])
      implicit val appleAppDataEncoder: Encoder[AppleAppData] = Encoders.bean[AppleAppData](classOf[AppleAppData])

      // On charge des données
      val header_google = googlePlayFile.first()
      val header_app = appleAppDataFile.first()

      val googlePlayData = googlePlayFile.filter(row => row != header_google)
      val appleAppData = appleAppDataFile.filter(row => row != header_app)

      // On transforme les données en RDDs de GooglePlay et AppleAppData
      val googlePlayCache = googlePlayData.map(s => GooglePlay.fromColumn(s.split(","))).rdd.cache()
      val appleAppDataCache = appleAppData.map(s => AppleAppData.fromColumn(s.split(","))).rdd.cache()

      // ************************************** 1. Remplacer le null par -1 ***************************************//

      // On remplace les valeurs nulles par -1 dans le dataset Google Play
      val filledGooglePlayCache = googlePlayCache.toDF().na.fill(-1)

      // On remplace les valeurs nulles par -1 dans le dataset Apple App Data
      val filledAppleAppDataCache = appleAppDataCache.toDF().na.fill(-1)

      // ************************************** 2. Supprimer certaines colonnes ************************************//

      // Colonnes à supprimer pour App Store
      val appStoreColumnsToRemove = Seq(
        "developerWebsite", "appStoreUrl", "developerid",
        "Developer", "developerUrl"
      )

      // Colonnes à supprimer pour Google Play Store
      val googlePlayColumnsToRemove = Seq(
        "developerid", "minimumInstalls", "developerWebsite",
        "developerEmail", "privacyPolicy"
      )

      // On supprime les colonnes spécifiées des DataFrames
      val cleanedAppleAppDataCache = filledAppleAppDataCache.toDF().drop(appStoreColumnsToRemove: _*)
      val cleanedGooglePlayCache = filledGooglePlayCache.toDF().drop(googlePlayColumnsToRemove: _*)

      // ************************************** 3. Jointure ************************************//

      // On convertie les RDD en DataFrames
      val cleanedAppleAppDF = cleanedAppleAppDataCache.toDF()
      val cleanedGooglePlayDF = cleanedGooglePlayCache.toDF()

      // *********************************** 3. Join DataFrames *********************************** //

      val cleanedAppleAppDFRenamed = cleanedAppleAppDF
        .withColumnRenamed("free", "free_appStore")
        .withColumnRenamed("appId", "appId_appStore")
        .withColumnRenamed("price", "price_appStore")
        .withColumnRenamed("currency", "currency_appStore")
       // .withColumnRenamed("developerid", "developerid_appStore")
       // .withColumnRenamed("developerWebsite", "developerWebsite_appStore")
        .withColumnRenamed("contentRating", "contentRating_appStore")
        .withColumnRenamed("released", "released_appStore")

      // On effectue la jointure sur la colonne "appName"
      val joinedDF = cleanedGooglePlayDF.join(cleanedAppleAppDFRenamed, Seq("appName"), "inner");

      // Table Google
      println("Tableau google")
      cleanedGooglePlayDF.show(3)

      // Table App Store
      println("Tableau apple")
      cleanedAppleAppDF.show(3)

      // Table de jointure
      println("Tableau joined")
      joinedDF.show(3)

      // voir le nombre de ligne de notre fichier
      val numRows = joinedDF.count()
      println(s"Le nombre total de lignes dans joinedDF est : $numRows")

      val repartition = joinedDF.repartition(1);
      repartition.write.parquet(args(2));

    } finally {

      // On Arrêt la session Spark
      spark.stop()
    }
  }


}