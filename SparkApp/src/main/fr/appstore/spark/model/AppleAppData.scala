package fr.appstore.spark.model


class AppleAppData extends Serializable {

    private var appId: String = null
    private var appName: String = null
    private var appStoreUrl: String = null
    private var primaryGenre: String = null // Categories
    private var contentRating: String = null
    private var sizeBytes: Long = 0L
    private var requiredIOSVersion: Double = 0.0
    private var released: String = null
    private var updated: String = null
    private var version: String = null
    private var price: Double = 0.0
    private var currency: String = null
    private var isFree: Boolean = false
    private var developerId: Long = 0L
    private var developer: String = null
    private var developerUrl: String = null
    private var developerWebsite: String = null
    private var averageUserRating: Double = 0.0
    private var reviews: Long = 0L
    private var currentVersionScore: Double = 0.0
    private var currentVersionReviews: Long = 0L

    def this(appId: String, appName: String, appStoreUrl: String, primaryGenre: String, contentRating: String,
             sizeBytes: Long, requiredIOSVersion: Double, released: String, updated: String, version: String,
             price: Double, currency: String, isFree: Boolean, developerId: Long, developer: String,
             developerUrl: String, developerWebsite: String, averageUserRating: Double, reviews: Long,
             currentVersionScore: Double, currentVersionReviews: Long) {
      this()
      this.appId = appId
      this.appName = appName
      this.appStoreUrl = appStoreUrl
      this.primaryGenre = primaryGenre
      this.contentRating = contentRating
      this.sizeBytes = sizeBytes
      this.requiredIOSVersion = requiredIOSVersion
      this.released = released
      this.updated = updated
      this.version = version
      this.price = price
      this.currency = currency
      this.isFree = isFree
      this.developerId = developerId
      this.developer = developer
      this.developerUrl = developerUrl
      this.developerWebsite = developerWebsite
      this.averageUserRating = averageUserRating
      this.reviews = reviews
      this.currentVersionScore = currentVersionScore
      this.currentVersionReviews = currentVersionReviews
    }

    // getter et le setter
    def getAppId: String = appId
    def setAppId(appId: String): Unit = {
      this.appId = appId
    }

    def getAppName: String = appName
    def setAppName(appName: String): Unit = {
      this.appName = appName
    }

    def getAppStoreUrl: String = appStoreUrl
    def setAppStoreUrl(appStoreUrl: String): Unit = {
      this.appStoreUrl = appStoreUrl
    }

    def getPrimaryGenre: String = primaryGenre
    def setPrimaryGenre(primaryGenre: String): Unit = {
      this.primaryGenre = primaryGenre
    }

    def getContentRating: String = contentRating
    def setContentRating(contentRating: String): Unit = {
      this.contentRating = contentRating
    }

    def getSizeBytes: Long = sizeBytes
    def setSizeBytes(sizeBytes: Long): Unit = {
      this.sizeBytes = sizeBytes
    }

    def getRequiredIOSVersion: Double = requiredIOSVersion
    def setRequiredIOSVersion(requiredIOSVersion: Double): Unit = {
      this.requiredIOSVersion = requiredIOSVersion
    }

    def getReleased: String = released
    def setReleased(released: String): Unit = {
      this.released = released
    }

    def getUpdated: String = updated
    def setUpdated(updated: String): Unit = {
      this.updated = updated
    }

    def getVersion: String = version
    def setVersion(version: String): Unit = {
      this.version = version
    }

    def getPrice: Double = price
    def setPrice(price: Double): Unit = {
      this.price = price
    }

    def getCurrency: String = currency
    def setCurrency(currency: String): Unit = {
      this.currency = currency
    }

    def getFree: Boolean = isFree
    def setFree(isFree: Boolean): Unit = {
      this.isFree = isFree
    }

    def getDeveloperId: Long = developerId
    def setDeveloperId(developerId: Long): Unit = {
      this.developerId = developerId
    }

    def getDeveloper: String = developer
    def setDeveloper(developer: String): Unit = {
      this.developer = developer
    }

    def getDeveloperUrl: String = developerUrl
    def setDeveloperUrl(developerUrl: String): Unit = {
      this.developerUrl = developerUrl
    }

    def getDeveloperWebsite: String = developerWebsite
    def setDeveloperWebsite(developerWebsite: String): Unit = {
      this.developerWebsite = developerWebsite
    }

    def getAverageUserRating: Double = averageUserRating
    def setAverageUserRating(averageUserRating: Double): Unit = {
      this.averageUserRating = averageUserRating
    }

    def getReviews: Long = reviews
    def setReviews(reviews: Long): Unit = {
      this.reviews = reviews
    }

    def getCurrentVersionScore: Double = currentVersionScore
    def setCurrentVersionScore(currentVersionScore: Double): Unit = {
      this.currentVersionScore = currentVersionScore
    }

    def getCurrentVersionReviews: Long = currentVersionReviews
    def setCurrentVersionReviews(currentVersionReviews: Long): Unit = {
      this.currentVersionReviews = currentVersionReviews
    }


  }

  object AppleAppData {

    def apply(appId: String, appName: String, appStoreUrl: String, primaryGenre: String, contentRating: String,
              sizeBytes: Long, requiredIOSVersion: Double, released: String, updated: String, version: String,
              price: Double, currency: String, isFree: Boolean, developerId: Long, developer: String,
              developerUrl: String, developerWebsite: String, averageUserRating: Double, reviews: Long,
              currentVersionScore: Double, currentVersionReviews: Long): AppleAppData = {

      new AppleAppData(appId, appName, appStoreUrl, primaryGenre, contentRating, sizeBytes, requiredIOSVersion,
        released, updated, version, price, currency, isFree, developerId, developer, developerUrl, developerWebsite,
        averageUserRating, reviews, currentVersionScore, currentVersionReviews)
    }

    def fromColumn(columns: Array[String]): AppleAppData = {

      new AppleAppData(
        parseText(columns, 0), parseText(columns, 1), parseText(columns, 2), parseText(columns, 3),
        parseText(columns, 4), parseLong(columns, 5), parseDouble(columns, 6), parseText(columns, 7),
        parseText(columns, 8), parseText(columns, 9), parseDouble(columns, 10), parseText(columns, 11),
        parseBoolean(columns, 12), parseLong(columns, 13), parseText(columns, 14), parseText(columns, 15),
        parseText(columns, 16), parseDouble(columns, 17), parseLong(columns, 18), parseDouble(columns, 19),
        parseLong(columns, 20)
      )
    }

    private def parseText(columns: Array[String], x: Int): String = columns(x)

    private def parseDouble(columns: Array[String], x: Int): Double = {
      val value = Option(columns(x)).getOrElse("0.0") // Si la valeur est null, utilisez "0.0" comme valeur par défaut
      try {
        value.toDouble
      } catch {
        case _: NumberFormatException => 0.0 // En cas d'échec de conversion, retournez 0.0
      }
    }

    private def parseLong(columns: Array[String], x: Int): Long = {
      val value = Option(columns(x)).getOrElse("0") // Si la valeur est null, utilisez "0" comme valeur par défaut
      try {
        value.toLong
      } catch {
        case _: NumberFormatException => 0L // En cas d'échec de conversion, on retourne 0L
      }
    }

    private def parseBoolean(columns: Array[String], x: Int): Boolean = {
      val value = Option(columns(x)).getOrElse("false") // Si la valeur est null,on utilise "false" comme valeur par défaut
      try {
        value.toBoolean
      } catch {
        case _: IllegalArgumentException => false // En cas d'échec de conversion, retournez false
      }
    }

}

