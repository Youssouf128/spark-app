package fr.appstore.spark.model

class GooglePlay extends Serializable {

  private var appName: String = null
  private var appId: String = null
  private var category: String = null
  private var rating: Double = 0.0
  private var ratingCount: Double = 0.0
  private var installs: String = null
  private var minimumInstalls: Double = 0.0
  private var maximumInstalls: Double = 0.0
  private var isFree: Boolean = false
  private var price: Double = 0.0
  private var currency: String = null
  private var size: String = null
  private var minimumAndroid: String = null
  private var developerId: String = null
  private var developerWebsite: String = null
  private var developerEmail: String = null
  private var released: String = null
  private var lastUpdated: String = null
  private var contentRating: String = null
  private var privacyPolicy: String = null
  private var adSupported: Boolean = false
  private var inAppPurchases: Boolean = false
  private var editorsChoice: Boolean = false
  private var scrapedTime: String = null

  def this(appName: String, appId: String, category: String, rating: Double, ratingCount: Double,
           installs: String, minimumInstalls: Double, maximumInstalls: Double, isFree: Boolean, price: Double,
           currency: String, size: String, minimumAndroid: String, developerId: String, developerWebsite: String,
           developerEmail: String, released: String, lastUpdated: String, contentRating: String, privacyPolicy: String,
           adSupported: Boolean, inAppPurchases: Boolean, editorsChoice: Boolean, scrapedTime: String) {
    this()
    this.appName = appName
    this.appId = appId
    this.category = category
    this.rating = rating
    this.ratingCount = ratingCount
    this.installs = installs
    this.minimumInstalls = minimumInstalls
    this.maximumInstalls = maximumInstalls
    this.isFree = isFree
    this.price = price
    this.currency = currency
    this.size = size
    this.minimumAndroid = minimumAndroid
    this.developerId = developerId
    this.developerWebsite = developerWebsite
    this.developerEmail = developerEmail
    this.released = released
    this.lastUpdated = lastUpdated
    this.contentRating = contentRating
    this.privacyPolicy = privacyPolicy
    this.adSupported = adSupported
    this.inAppPurchases = inAppPurchases
    this.editorsChoice = editorsChoice
    this.scrapedTime = scrapedTime
  }

  // getter et le setter
  def getAppName: String = appName
  def setAppName(appName: String): Unit = { this.appName = appName }

  def getAppId: String = appId
  def setAppId(appId: String): Unit = { this.appId = appId }

  def getCategory: String = category
  def setCategory(category: String): Unit = { this.category = category }

  def getRating: Double = rating
  def setRating(rating: Double): Unit = { this.rating = rating }

  def getRatingCount: Double = ratingCount
  def setRatingCount(ratingCount: Double): Unit = { this.ratingCount = ratingCount }

  def getInstalls: String = installs
  def setInstalls(installs: String): Unit = { this.installs = installs }

  def getMinimumInstalls: Double = minimumInstalls
  def setMinimumInstalls(minimumInstalls: Double): Unit = { this.minimumInstalls = minimumInstalls }

  def getMaximumInstalls: Double = maximumInstalls
  def setMaximumInstalls(maximumInstalls: Double): Unit = { this.maximumInstalls = maximumInstalls }

  def getFree: Boolean = isFree

  def setFree(free: Boolean): Unit = { this.isFree = free }

  def getPrice: Double = price
  def setPrice(price: Double): Unit = { this.price = price }

  def getCurrency: String = currency
  def setCurrency(currency: String): Unit = { this.currency = currency }

  def getSize: String = size
  def setSize(size: String): Unit = { this.size = size }

  def getMinimumAndroid: String = minimumAndroid
  def setMinimumAndroid(minimumAndroid: String): Unit = { this.minimumAndroid = minimumAndroid }

  def getDeveloperId: String = developerId
  def setDeveloperId(developerId: String): Unit = { this.developerId = developerId }

  def getDeveloperWebsite: String = developerWebsite
  def setDeveloperWebsite(developerWebsite: String): Unit = { this.developerWebsite = developerWebsite }

  def getDeveloperEmail: String = developerEmail
  def setDeveloperEmail(developerEmail: String): Unit = { this.developerEmail = developerEmail }

  def getReleased: String = released
  def setReleased(released: String): Unit = { this.released = released }

  def getLastUpdated: String = lastUpdated
  def setLastUpdated(lastUpdated: String): Unit = { this.lastUpdated = lastUpdated }

  def getContentRating: String = contentRating
  def setContentRating(contentRating: String): Unit = { this.contentRating = contentRating }

  def getPrivacyPolicy: String = privacyPolicy
  def setPrivacyPolicy(privacyPolicy: String): Unit = { this.privacyPolicy = privacyPolicy }

  def isAdSupported: Boolean = adSupported
  def setAdSupported(adSupported: Boolean): Unit = { this.adSupported = adSupported }

  def isInAppPurchases: Boolean = inAppPurchases
  def setInAppPurchases(inAppPurchases: Boolean): Unit = { this.inAppPurchases = inAppPurchases }

  def isEditorsChoice: Boolean = editorsChoice
  def setEditorsChoice(editorsChoice: Boolean): Unit = { this.editorsChoice = editorsChoice }

  def getScrapedTime: String = scrapedTime
  def setScrapedTime(scrapedTime: String): Unit = { this.scrapedTime = scrapedTime }


}

object GooglePlay {
  def apply(appName: String, appId: String, category: String, rating: Double, ratingCount: Double,
            installs: String, minimumInstalls: Double, maximumInstalls: Double, isFree: Boolean, price: Double,
            currency: String, size: String, minimumAndroid: String, developerId: String, developerWebsite: String,
            developerEmail: String, released: String, lastUpdated: String, contentRating: String, privacyPolicy: String,
            adSupported: Boolean, inAppPurchases: Boolean, editorsChoice: Boolean, scrapedTime: String): GooglePlay = {

    new GooglePlay(appName, appId, category, rating, ratingCount, installs, minimumInstalls, maximumInstalls, isFree,
      price, currency, size, minimumAndroid, developerId, developerWebsite, developerEmail, released, lastUpdated,
      contentRating, privacyPolicy, adSupported, inAppPurchases, editorsChoice, scrapedTime)
  }

  def fromColumn(columns: Array[String]): GooglePlay = {
    new GooglePlay(
      parseText(columns, 0), parseText(columns, 1), parseText(columns, 2), parseDouble(columns, 3),
      parseDouble(columns, 4), parseText(columns, 5), parseDouble(columns, 6), parseDouble(columns, 7),
      parseBoolean(columns, 8), parseDouble(columns, 9), parseText(columns, 10), parseText(columns, 11),
      parseText(columns, 12), parseText(columns, 13), parseText(columns, 14), parseText(columns, 15),
      parseText(columns, 16), parseText(columns, 17), parseText(columns, 18), parseText(columns, 19),
      parseBoolean(columns, 20), parseBoolean(columns, 21), parseBoolean(columns, 22), parseText(columns, 23)
    )
  }

  private def parseText(columns: Array[String], x: Int): String = columns(x)

  private def parseDouble(columns: Array[String], x: Int): Double = {
    val value = Option(columns(x)).getOrElse("0.0") // Si la valeur est null, utilisez "0.0" comme valeur par défaut
    try {
      value.toDouble
    } catch {
      case _: NumberFormatException => 0.0 // En cas d'échec de conversion, retournez 0.0
    }
  }
  private def parseLong(columns: Array[String], x: Int): Long = {
    val value = Option(columns(x)).getOrElse("0") // Si la valeur est null, utilisez "0" comme valeur par défaut
    try {
      value.toLong
    } catch {
      case _: NumberFormatException => 0L // En cas d'échec de conversion, retournez 0L
    }
  }

  private def parseBoolean(columns: Array[String], x: Int): Boolean = {
    val value = Option(columns(x)).getOrElse("false") // Si la valeur est null, utilisez "false" comme valeur par défaut
    try {
      value.toBoolean
    } catch {
      case _: IllegalArgumentException => false // En cas d'échec de conversion, retournez false
    }
  }
}
