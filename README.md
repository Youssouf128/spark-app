<h1 align="center">App Spark</h1>
<p align="center">Youssouf ABAYAZID & Nihal BOUCHLAGHMI</p>
<p align="center">
    <a href="https://www.kaggle.com/datasets/gauthamp10/google-playstore-apps">
        Dataset Google PlayStore sur Kaggle
    </a><br>
    <a href="https://www.kaggle.com/gauthamp10/apple-appstore-apps">
         Dataset Apple App Store sur Kaggle
    </a>
</p>

## Description

Pour notre projet, nous avons choisi un ensemble de données composé des fichiers google-playstore-apps.csv et apple-appstore-apps.csv. La taille totale de ce jeu de données est de 1,1 Go.

Voir le dataset : télécharger les données : 

   <a href="https://drive.google.com/file/d/10_2AhTgDE3qmtzXGFM7fCgINk1-A4GGm/view?usp=drive_link">
        Dataset Google PlayStore sur Drive
    </a><br>
    <a href="https://drive.google.com/file/d/17t0O5QEAJE-BiSXHXVWtNe9piGh-moAx/view?usp=drive_link">
         Dataset Apple App Store sur Drive
    </a>

## Compilation

Pour compiler le projet, il suffit de lancer la commande suivante :

```bash
mvn clean compile assembly:single -U
```
ou 

```bash
mvn clean install
```

## Machine Virtuel

### COMMANDES

On démarre tous les démons Hadoop sur la machine virtuel . Cela inclut le NameNode, DataNode, ResourceManager, NodeManager, et d'autres composants du framework Hadoop.
```bash
start-all.sh
```

On crée un répertoire appelé "spark-logs" dans le système de fichiers distribué HDFS.

```bash
hdfs dfs -mkdir /spark-logs 
```

On démarre le serveur d'historique Spark, qui permet de consulter les journaux et l'historique des applications Spark.

```bash
$SPARK_HOME/sbin/start-history-server.sh
```

## Exécution

Pour exécuter le projet, il suffit de lancer la commande suivante :

```bash
spark-submit --deploy-mode client --class fr.appstore.spark.Main SparkApp-1.0-SNAPSHOT.jar <chemin sur le HDFS du dataset des Google playstore > <chemin sur le HDFS du dataset des appleAppData> <chemin sur le HDFS pour l\'exporation parquet>
```

Exemple :

```bash
spark-submit --deploy-mode client --class fr.appstore.spark.Main SparkApp-1.0-SNAPSHOT.jar hdfs://0.0.0.0:9000/Spark_app_data/Google-Playstore.csv hdfs://0.0.0.0:9000/Spark_app_data/appleAppData.csv hdfs://0.0.0.0:9000/spark_parquet
```

## Récupération de données 


On récupère de HDFS le contenu du répertoire "spark_parquet" (spark_parquet contient le fichiers parquet ) vers le système de fichiers local.

```bash
hdfs dfs -get hdfs://0.0.0.0:9000/spark_parquet/ ./ 
```

On utilise SCP (Secure Copy) pour copier de manière récursive le répertoire "spark_parquet" depuis la machine virtuelle (VM) locale vers le répertoire local actuel. 

Le paramètre -P spécifie le port SSH

```bash
scp -P 2222 -r student@localhost:~/spark_parquet ./
```
## Structure du projet

Le projet est composé de 3 packages :

- `fr.appstore.spark.model` : contient les classes représentant les données du dataset
- `fr.appstore.spark` : contient le point d'entrée du programme


## Auteurs

- Youssouf ABAYAZID
- Nihal BOUCHLAGHMI
